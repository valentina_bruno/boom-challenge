# Notes on the implementation

## Functional behaviors
In addition to the requirements I implemented the following behaviors: 
* An order can be rescheduled as long as its state is PENDING or SCHEDULED, so that it is possible to change the schedule in case of mistakes
* A photographer can be assigned to an order as long as its state is SCHEDULED or ASSIGNED, for the same reason explained above
* When creating an order the contact is saved in a separate table, so that subsequent order creations can reuse the same contact by specifying its id. If the client wants to create a new contact instead, it should not send contact id in the request.
* The order keeps track of all the zip files uploaded until approval. Every time the operator rejects the uploaded file it is marked as unapproved but it is not removed from the order. 
  It is not possible to upload a new zip file until the previous one has received a feedback from the operator.
  A completed order will then contain the history of all rejected photoshoots and only one approved photoshoot (the last uploaded).

## Technical notes
* I did not write an endpoint to import photographers but I added a sql script that is executed at startup populating the db with 3 photographers (ids 1,2,3).
  I thought it was more practical!
* In order to perform validation on business hours it is necessary to send both the datetime (utc) and the timezone of the order. 
The check is performed with respect to the order's timezone.
* I wrote tests covering controller, service and domain logic, which I consider fundamental. 
  I didn't have time to write extensive tests on validations, I just wrote tests for the custom schedule validator. Anyway If this was a production project I would write unit tests for all aspects of the validation requirements, to make sure that I didn't miss any check.

# How to build and run the application
1. Make sure you have Java 11 and Maven installed (in alternative you can use the Maven wrapper contained inside the project)
2. It is recommended to have Lombok plugin installed on your IDE (https://projectlombok.org/)
3. Enter the project root folder and execute from the command line
   `mvn install spring-boot:run`
   (or `mvnw install spring-boot:run` if using Maven wrapper) 

# REST services documentation

The API documentation is generated on the fly during application startup.
Open `http://localhost:8080/swagger-ui/index.html` to visualize and interact with the API’s resources.
