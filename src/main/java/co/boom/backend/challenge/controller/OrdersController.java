package co.boom.backend.challenge.controller;

import co.boom.backend.challenge.domain.Order;
import co.boom.backend.challenge.domain.Schedule;
import co.boom.backend.challenge.dto.CreateOrderDto;
import co.boom.backend.challenge.dto.OrderApprovalDto;
import co.boom.backend.challenge.dto.PhotographerAssignmentDto;
import co.boom.backend.challenge.dto.ScheduleDto;
import co.boom.backend.challenge.service.OrdersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@RestController
@RequestMapping("orders")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createOrder(@RequestBody @Valid CreateOrderDto createOrderDto) {

        Order order = modelMapper.map(createOrderDto, Order.class);
        var createdOrder = ordersService.createOrder(order);

        return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{id}")
                .buildAndExpand(createdOrder.getId()).toUri())
                .build();
    }

    @GetMapping(path = "{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Order findOrderById(@PathVariable long orderId) {
        return ordersService.findOrderById(orderId);
    }

    @PutMapping(path = "{orderId}/schedule", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSchedule(@PathVariable long orderId, @RequestBody @Valid ScheduleDto scheduleDto) {
        Schedule schedule = modelMapper.map(scheduleDto, Schedule.class);
        ordersService.updateSchedule(orderId, schedule);
    }

    @PutMapping(path = "{orderId}/photographer", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void assignPhotographer(@PathVariable long orderId,
                                   @RequestBody @Valid PhotographerAssignmentDto photographerAssignmentDto) {
        ordersService.assignPhotographer(orderId, photographerAssignmentDto.getId());
    }

    @PostMapping(path = "{orderId}/photoshoots")
    @ResponseStatus(HttpStatus.CREATED)
    public void uploadPhotoshoot(@PathVariable long orderId, @RequestPart(value = "file") MultipartFile file) {
        ordersService.uploadPhotoshoot(orderId, file);
    }

    @PostMapping(path = "{orderId}/approval")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void registerApproval(@PathVariable long orderId, @RequestBody @Valid OrderApprovalDto approvalDto) {
        ordersService.registerApproval(orderId, approvalDto.approved);
    }

    @DeleteMapping(path = "{orderId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelOrder(@PathVariable long orderId) {
        ordersService.cancel(orderId);
    }

}
