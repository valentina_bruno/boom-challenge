package co.boom.backend.challenge.repository;

import co.boom.backend.challenge.domain.Photographer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotographersRepository extends JpaRepository<Photographer, Long> {
}
