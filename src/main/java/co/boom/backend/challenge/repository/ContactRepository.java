package co.boom.backend.challenge.repository;

import co.boom.backend.challenge.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact, Long> {
}
