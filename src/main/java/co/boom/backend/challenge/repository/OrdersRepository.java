package co.boom.backend.challenge.repository;

import co.boom.backend.challenge.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface OrdersRepository extends JpaRepository<Order, Long> {

    @Query("select o from Order o left join fetch o.photoshoots where o.id = :id")
    Optional<Order> findFullOrderById(long id);
}
