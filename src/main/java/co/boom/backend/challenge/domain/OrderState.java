package co.boom.backend.challenge.domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum OrderState {
    UNSCHEDULED,
    PENDING,
    ASSIGNED,
    UPLOADED,
    COMPLETED,
    CANCELED;

    private static final List<OrderState> updateScheduleAllowedStates = Arrays.asList(UNSCHEDULED, PENDING);
    private static final List<OrderState> assignPhotographerAllowedStates = Arrays.asList(PENDING, ASSIGNED);
    private static final List<OrderState> uploadAllowedStates = Collections.singletonList(ASSIGNED);
    private static final List<OrderState> approvalAllowedStates = Collections.singletonList(UPLOADED);

    public boolean allowsUpdateSchedule() {
        return updateScheduleAllowedStates.contains(this);
    }

    public boolean allowsAssignPhotographer() {
        return assignPhotographerAllowedStates.contains(this);
    }

    public boolean allowsUploadPhotoshoot() {
        return uploadAllowedStates.contains(this);
    }

    public boolean allowsApproval() {
        return approvalAllowedStates.contains(this);
    }
}
