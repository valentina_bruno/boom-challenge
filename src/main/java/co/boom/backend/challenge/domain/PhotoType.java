package co.boom.backend.challenge.domain;

public enum PhotoType {
    RealEstate,
    Food,
    Events
}
