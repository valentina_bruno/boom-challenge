package co.boom.backend.challenge.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.time.LocalDateTime;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Photoshoot {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String zipLocation;

    private Boolean approved;

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime modifiedDate;

    public static Photoshoot create(URL zipLocation) {
        Photoshoot photoshoot = new Photoshoot();
        photoshoot.zipLocation = zipLocation.toString();
        return photoshoot;
    }
}
