package co.boom.backend.challenge.domain;

import co.boom.backend.challenge.common.validation.ScheduleValidation;
import co.boom.backend.challenge.common.validation.SupportsScheduleValidation;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Embeddable
@Getter
@Setter
@ScheduleValidation
public class Schedule implements SupportsScheduleValidation {

    @NotNull
    private LocalDateTime dateTime;

    @NotEmpty
    private String timezone;
}
