package co.boom.backend.challenge.domain;

import co.boom.backend.challenge.common.exceptions.DomainException;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static co.boom.backend.challenge.domain.OrderState.*;

@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "ORDERS")
public class Order {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Valid
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Contact contact;

    @ManyToOne
    private Photographer photographer;

    @Enumerated(EnumType.STRING)
    @NotNull
    private PhotoType photoType;

    @Enumerated(EnumType.STRING)
    private OrderState state;

    private String title;

    private String logisticInfo;

    @Valid
    private Schedule schedule;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn
    @OrderBy("createdDate")
    @Valid
    private List<Photoshoot> photoshoots = new ArrayList<>();

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime modifiedDate;

    public void init() {
        if (id != null) {
            throw new DomainException("Cannot init persistent order");
        }
        if (schedule == null) {
            state = UNSCHEDULED;
        } else {
            state = PENDING;
        }
    }

    public void updateSchedule(Schedule schedule) {
        if (!state.allowsUpdateSchedule()) {
            throw new DomainException(String.format("Update schedule not allowed when order state is %s", state.name()));
        }
        this.schedule = schedule;
        this.state = PENDING;
    }

    public void assignPhotographer(Photographer photographer) {
        if (!state.allowsAssignPhotographer()) {
            throw new DomainException(String.format("Assign photographer not allowed when order state is %s", state.name()));
        }
        this.photographer = photographer;
        this.state = ASSIGNED;
    }

    public void addPhotoshoot(Photoshoot photoshoot) {
        if (!state.allowsUploadPhotoshoot()) {
            throw new DomainException(String.format("Adding photoshoot not allowed when order state is %s", state.name()));
        }
        photoshoots.add(photoshoot);
        state = UPLOADED;
    }

    public void approve() {
        approveLastUploadedPhotoshoot(true);
        state = COMPLETED;
    }

    public void reject() {
        approveLastUploadedPhotoshoot(false);
        state = ASSIGNED;
    }

    private void approveLastUploadedPhotoshoot(boolean approved) {
        if (!state.allowsApproval()) {
            throw new DomainException(String.format("Order approval/rejection not allowed when order state is %s", state.name()));
        }
        var lastPhotoshoot = photoshoots.get(photoshoots.size() - 1);
        lastPhotoshoot.setApproved(approved);
    }

    public void cancel() {
        state = CANCELED;
    }
}
