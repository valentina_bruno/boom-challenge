package co.boom.backend.challenge.service;

import co.boom.backend.challenge.common.exceptions.DomainException;
import co.boom.backend.challenge.common.exceptions.DomainNotFoundException;
import co.boom.backend.challenge.domain.*;
import co.boom.backend.challenge.repository.ContactRepository;
import co.boom.backend.challenge.repository.OrdersRepository;
import co.boom.backend.challenge.repository.PhotographersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;

@Service
@Transactional
public class OrdersService {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private PhotographersRepository photographersRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private PhotoStorageService photoStorageService;

    public Order createOrder(Order order) {
        Long contactId = order.getContact().getId();
        if (contactId != null) {
            Contact contact = contactRepository.findById(contactId)
                    .orElseThrow(() -> new DomainException("Invalid contact id"));
            order.setContact(contact);
        }

        order.init();
        return ordersRepository.save(order);
    }

    public Order findOrderById(long orderId) {
        return ordersRepository.findFullOrderById(orderId).orElseThrow(DomainNotFoundException::new);
    }

    public void updateSchedule(long orderId, Schedule schedule) {
        var order = findOrderById(orderId);
        order.updateSchedule(schedule);
    }

    public void assignPhotographer(long orderId, long photographerId) {
        Photographer photographer = photographersRepository.findById(photographerId)
                .orElseThrow(() -> new DomainException("Invalid photographer id"));

        var order = findOrderById(orderId);
        order.assignPhotographer(photographer);
    }

    public void uploadPhotoshoot(long orderId, MultipartFile zipFile) {
        Order order = findOrderById(orderId);
        if (!order.getState().allowsUploadPhotoshoot()) {
            throw new DomainException(String.format("Photoshoot cannot be uploaded when order is in state %s", order.getState()));
        }

        URL zipUrl = photoStorageService.uploadPhotoshoot(zipFile);
        order.addPhotoshoot(Photoshoot.create(zipUrl));
    }

    public void registerApproval(long orderId, boolean approved) {
        Order order = findOrderById(orderId);
        if (approved) {
            order.approve();
        } else {
            order.reject();
        }
    }

    public void cancel(long orderId) {
        Order order = findOrderById(orderId);
        order.cancel();
    }
}
