package co.boom.backend.challenge.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.MalformedURLException;
import java.net.URL;

@Service
public class PhotoStorageService {

    public URL uploadPhotoshoot(MultipartFile zipFile) {
        try {
            return new URL(String.format("http://fakeUrl/%s", zipFile.getOriginalFilename()));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
