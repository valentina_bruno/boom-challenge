package co.boom.backend.challenge.common.validation;

import java.time.LocalDateTime;

public interface SupportsScheduleValidation {

    public LocalDateTime getDateTime();

    public String getTimezone();
}
