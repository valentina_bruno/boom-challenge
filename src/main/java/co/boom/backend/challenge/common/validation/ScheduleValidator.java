package co.boom.backend.challenge.common.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalTime;
import java.time.ZoneId;

public class ScheduleValidator implements ConstraintValidator<ScheduleValidation, SupportsScheduleValidation> {

    private final static LocalTime businessHoursStart = LocalTime.of(8, 0);
    private final static LocalTime businessHoursEnd = LocalTime.of(20, 0);

    private final static TimezoneValidator timezoneValidator = new TimezoneValidator();

    @Override
    public boolean isValid(SupportsScheduleValidation schedule, ConstraintValidatorContext constraintValidatorContext) {

        boolean timezoneValid = timezoneValidator.isValid(schedule.getTimezone(), constraintValidatorContext);
        if (!timezoneValid) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("Schedule timezone is not valid")
                    .addPropertyNode("timezone").addConstraintViolation();
            return false;
        }

        boolean isWithinBusinessHours = isWithinBusinessHours(schedule);
        if (!isWithinBusinessHours) {
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate("Schedule time is not within business hours")
                    .addPropertyNode("dateTime").addConstraintViolation();
        }

        return isWithinBusinessHours;
    }

    private boolean isWithinBusinessHours(SupportsScheduleValidation schedule) {
        var utcDateTime = schedule.getDateTime().atZone(ZoneId.of("UTC"));
        var zonedDateTime = utcDateTime.withZoneSameInstant(ZoneId.of(schedule.getTimezone()));
        LocalTime scheduleTime = zonedDateTime.toLocalTime();
        return !scheduleTime.isBefore(businessHoursStart) && !scheduleTime.isAfter(businessHoursEnd);
    }
}
