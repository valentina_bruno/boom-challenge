package co.boom.backend.challenge.common.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ScheduleValidator.class)
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ScheduleValidation {

    String message() default "Invalid schedule";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
