package co.boom.backend.challenge.common.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.ZoneId;

public class TimezoneValidator implements ConstraintValidator<TimezoneValidation, String> {

    @Override
    public boolean isValid(String timezone, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = true;
        try {
            ZoneId.of(timezone);
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;
    }
}
