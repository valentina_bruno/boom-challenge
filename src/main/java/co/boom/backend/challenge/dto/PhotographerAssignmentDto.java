package co.boom.backend.challenge.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PhotographerAssignmentDto {

    @NotNull
    private Long id;
}
