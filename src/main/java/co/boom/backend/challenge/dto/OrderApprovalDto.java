package co.boom.backend.challenge.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderApprovalDto {

    public boolean approved;
}
