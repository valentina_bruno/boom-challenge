package co.boom.backend.challenge.dto;

import co.boom.backend.challenge.common.validation.ScheduleValidation;
import co.boom.backend.challenge.common.validation.SupportsScheduleValidation;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@ScheduleValidation
public class ScheduleDto implements SupportsScheduleValidation {

    @NotNull
    private LocalDateTime dateTime;

    @NotEmpty
    private String timezone;
}
