package co.boom.backend.challenge.dto;

import co.boom.backend.challenge.common.validation.ValueOfEnum;
import co.boom.backend.challenge.domain.PhotoType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class CreateOrderDto {

    @Valid
    private ContactDto contact;

    @NotEmpty
    @ValueOfEnum(enumClass = PhotoType.class)
    private String photoType;

    private String title;

    private String logisticInfo;

    @Valid
    private ScheduleDto schedule;
}
