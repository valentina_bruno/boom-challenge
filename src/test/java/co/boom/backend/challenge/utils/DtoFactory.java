package co.boom.backend.challenge.utils;

import co.boom.backend.challenge.domain.PhotoType;
import co.boom.backend.challenge.domain.Photographer;
import co.boom.backend.challenge.dto.*;

import java.time.LocalDateTime;

public class DtoFactory {

    public static CreateOrderDto createUnscheduledOrderDto() {
        CreateOrderDto order = new CreateOrderDto();
        order.setContact(createContactDto());
        order.setTitle("title");
        order.setLogisticInfo("logisticInfo");
        order.setPhotoType(PhotoType.Events.name());
        return order;
    }

    public static CreateOrderDto createPendingOrderDto() {
        CreateOrderDto order = createUnscheduledOrderDto();
        order.setSchedule(createScheduleDto());
        return order;
    }

    public static ScheduleDto createScheduleDto() {
        ScheduleDto schedule = new ScheduleDto();
        schedule.setDateTime(LocalDateTime.of(2021, 2, 27, 10, 0));
        schedule.setTimezone("Europe/Rome");
        return schedule;
    }

    public static ContactDto createContactDto() {
        ContactDto contact = new ContactDto();
        contact.setName("Name");
        contact.setSurname("Surname");
        contact.setEmail("challenge@boom.co");
        contact.setPhoneNumber("123456789");
        return contact;
    }

    public static PhotographerAssignmentDto createPhotographerAssignmentDto(long id) {
        PhotographerAssignmentDto photographerAssignmentDto = new PhotographerAssignmentDto();
        photographerAssignmentDto.setId(id);
        return photographerAssignmentDto;
    }

    public static OrderApprovalDto createApprovalDto(boolean approved) {
        OrderApprovalDto orderApprovalDto = new OrderApprovalDto();
        orderApprovalDto.setApproved(approved);
        return orderApprovalDto;
    }
}
