package co.boom.backend.challenge.utils;

import co.boom.backend.challenge.domain.*;

import java.time.LocalDateTime;

import static co.boom.backend.challenge.domain.OrderState.*;

public class DomainFactory {

    public static Order createUnscheduledOrder() {
        Order order = new Order();
        order.setContact(createContact());
        order.setTitle("title");
        order.setLogisticInfo("logisticInfo");
        order.setPhotoType(PhotoType.Events);
        order.setState(UNSCHEDULED);
        return order;
    }

    public static Order createPendingOrder() {
        Order order = createUnscheduledOrder();
        order.setSchedule(createSchedule());
        order.setState(PENDING);
        return order;
    }

    public static Order createAssignedOrder(Photographer photographer) {
        Order order = createPendingOrder();
        order.setPhotographer(photographer);
        order.setState(ASSIGNED);
        return order;
    }

    public static Order createUploadedOrder(Photographer photographer) {
        Order order = createAssignedOrder(photographer);
        order.getPhotoshoots().add(createPhotoshoot());
        order.setState(UPLOADED);
        return order;
    }

    public static Schedule createSchedule() {
        Schedule schedule = new Schedule();
        schedule.setDateTime(LocalDateTime.of(2021, 2, 27, 10, 0));
        schedule.setTimezone("Europe/Rome");
        return schedule;
    }

    public static Contact createContact() {
        Contact contact = new Contact();
        contact.setName("Name");
        contact.setSurname("Surname");
        contact.setEmail("challenge@boom.co");
        contact.setPhoneNumber("123456789");
        return contact;
    }

    public static Photographer createPhotographer() {
        Photographer photographer = new Photographer();
        photographer.setName("John");
        photographer.setSurname("Smith");
        return photographer;
    }

    public static Photoshoot createPhotoshoot() {
        Photoshoot photoshoot = new Photoshoot();
        photoshoot.setZipLocation("http://location");
        return photoshoot;
    }
}
