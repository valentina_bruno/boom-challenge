package co.boom.backend.challenge.utils;

import co.boom.backend.challenge.domain.Contact;
import co.boom.backend.challenge.domain.Order;
import co.boom.backend.challenge.domain.Schedule;
import co.boom.backend.challenge.dto.ContactDto;
import co.boom.backend.challenge.dto.CreateOrderDto;
import co.boom.backend.challenge.dto.ScheduleDto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class AssertionsUtils {

    public static void assertAreEqual(CreateOrderDto expected, Order actual) {
        assertEquals(expected.getTitle(), actual.getTitle());
        assertEquals(expected.getLogisticInfo(), actual.getLogisticInfo());
        assertAreEqual(expected.getContact(), actual.getContact());
        assertAreEqual(expected.getSchedule(), actual.getSchedule());
        assertEquals(expected.getPhotoType(), actual.getPhotoType().name());
    }

    private static void assertAreEqual(ScheduleDto expected, Schedule actual) {
        if(expected == null) {
            assertNull(actual);
        } else {
            assertEquals(expected.getDateTime(), actual.getDateTime());
            assertEquals(expected.getTimezone(), actual.getTimezone());
        }
    }

    public static void assertAreEqual(ContactDto expected, Contact actual) {
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getSurname(), actual.getSurname());
        assertEquals(expected.getPhoneNumber(), actual.getPhoneNumber());
        assertEquals(expected.getEmail(), actual.getEmail());
    }
}