package co.boom.backend.challenge.service;

import co.boom.backend.challenge.common.exceptions.DomainException;
import co.boom.backend.challenge.common.exceptions.DomainNotFoundException;
import co.boom.backend.challenge.domain.Contact;
import co.boom.backend.challenge.domain.Order;
import co.boom.backend.challenge.domain.Photographer;
import co.boom.backend.challenge.repository.ContactRepository;
import co.boom.backend.challenge.repository.OrdersRepository;
import co.boom.backend.challenge.repository.PhotographersRepository;
import co.boom.backend.challenge.utils.DomainFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.net.MalformedURLException;
import java.net.URL;

import static co.boom.backend.challenge.domain.OrderState.*;
import static co.boom.backend.challenge.utils.DomainFactory.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest
public class OrdersServiceTests {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private ContactRepository contactsRepository;

    @Autowired
    private PhotographersRepository photographersRepository;

    @MockBean
    private PhotoStorageService photoStorageService;

    private MultipartFile mockMultipartFile;

    @BeforeEach
    public void beforeEach() throws MalformedURLException {
        when(photoStorageService.uploadPhotoshoot(any())).thenReturn(new URL("http://location.com"));
        mockMultipartFile = new MockMultipartFile("file", (byte[]) null);
    }

    @AfterEach
    public void afterEach() {
        ordersRepository.deleteAll();
        contactsRepository.deleteAll();
        photographersRepository.deleteAll();
    }

    @Test
    public void createOrder_whenOrderNotValid_throwsException() {
        Order order = DomainFactory.createUnscheduledOrder();
        order.setPhotoType(null);
        assertThrows(Exception.class, () -> ordersService.createOrder(order));
    }

    @Test
    public void createOrder_whenOrderValid_createsOrderAndContact() {
        Order order = DomainFactory.createUnscheduledOrder();
        ordersService.createOrder(order);

        assertEquals(1, ordersRepository.count());
        assertEquals(1, contactsRepository.count());
    }

    @Test
    public void createOrder_whenOrderWithoutSchedule_createsUnscheduledOrder() {
        Order order = DomainFactory.createUnscheduledOrder();
        Order savedOrder = ordersService.createOrder(order);

        Order orderFromDb = ordersRepository.findById(savedOrder.getId()).get();
        assertEquals(UNSCHEDULED, orderFromDb.getState());
    }

    @Test
    public void createOrder_whenOrderWithSchedule_createsPendingOrder() {
        Order order = DomainFactory.createPendingOrder();
        Order savedOrder = ordersService.createOrder(order);

        Order orderFromDb = ordersRepository.findById(savedOrder.getId()).get();
        assertEquals(PENDING, orderFromDb.getState());
    }

    @Test
    public void createOrder_whenContactHasIdButDoesNotExist_throwsDomainException() {
        Order order = DomainFactory.createPendingOrder();
        order.getContact().setId(10L);

        assertThrows(DomainException.class, () -> ordersService.createOrder(order));
    }

    @Test
    public void createOrder_whenContactHasValidId_linksExistingContactToOrder() {
        Contact contact = DomainFactory.createContact();
        contactsRepository.save(contact);

        Order order = DomainFactory.createPendingOrder();
        order.getContact().setId(contact.getId());

        Order savedOrder = ordersService.createOrder(order);

        Order orderFromDb = ordersRepository.findById(savedOrder.getId()).get();
        assertNotNull(orderFromDb.getContact());
        assertEquals(contact.getId(), orderFromDb.getContact().getId());
        assertEquals(1, contactsRepository.count());
    }

    @Test
    public void findOrderById_whenOrderNotFound_throwsDomainNotFoundException() {
        assertThrows(DomainNotFoundException.class, () -> ordersService.findOrderById(10));
    }

    @Test
    public void findOrderById_whenOrderFound_returnsOrder() {
        Order savedOrder = ordersRepository.save(DomainFactory.createPendingOrder());
        Order orderById = ordersService.findOrderById(savedOrder.getId());
        assertNotNull(orderById);
    }

    @Test
    public void updateSchedule_whenOrderNotFound_throwsDomainNotFoundException() {
        assertThrows(DomainNotFoundException.class, () -> ordersService.updateSchedule(10, DomainFactory.createSchedule()));
    }

    @Test
    public void updateSchedule_whenOrderFound_savesOrderScheduleAndMovesOrderToPending() {
        Order savedOrder = ordersRepository.save(DomainFactory.createUnscheduledOrder());

        ordersService.updateSchedule(savedOrder.getId(), DomainFactory.createSchedule());

        Order orderFromDb = ordersRepository.findById(savedOrder.getId()).get();
        assertNotNull(orderFromDb.getSchedule());
        assertEquals(PENDING, orderFromDb.getState());
    }

    @Test
    public void assignPhotographer_whenOrderNotFound_throwsDomainNotFoundException() {
        Photographer photographer = photographersRepository.save(createPhotographer());
        assertThrows(DomainNotFoundException.class, () -> ordersService.assignPhotographer(10, photographer.getId()));
    }

    @Test
    public void assignPhotographer_whenPhotographerNotFound_throwsDomainException() {
        assertThrows(DomainException.class, () -> ordersService.assignPhotographer(10, 1000));
    }

    @Test
    public void assignPhotographer_whenPhotographerExists_assignsPhotographerAndMovesOrderToAssigned() {
        Photographer photographer = photographersRepository.save(createPhotographer());
        Order order = ordersRepository.save(createPendingOrder());

        ordersService.assignPhotographer(order.getId(), photographer.getId());

        Order orderFromDb = ordersRepository.findById(order.getId()).get();
        assertNotNull(orderFromDb.getPhotographer());
        assertEquals(photographer.getId(), orderFromDb.getPhotographer().getId());
        assertEquals(ASSIGNED, orderFromDb.getState());
    }

    @Test
    public void uploadPhotoshoot_whenOrderNotFound_throwsDomainNotFoundException() {
        assertThrows(DomainNotFoundException.class, () -> ordersService.uploadPhotoshoot(10, new MockMultipartFile("file", (byte[]) null)));
    }

    @Test
    public void uploadPhotoshoot_whenWrongState_throwsDomainExceptionAndDoesNotUploadZip() {
        Order order = ordersRepository.save(createPendingOrder());
        assertThrows(DomainException.class, () -> ordersService.uploadPhotoshoot(order.getId(), mockMultipartFile));
        verify(photoStorageService, times(0)).uploadPhotoshoot(mockMultipartFile);
    }

    @Test
    public void uploadPhotoshoot_whenOrderAssigned_uploadsZipAndSavesItInsideOrderAndMovesOrderToUploaded() {
        Photographer photographer = photographersRepository.save(createPhotographer());
        Order order = ordersRepository.save(createAssignedOrder(photographer));
        ordersService.uploadPhotoshoot(order.getId(), mockMultipartFile);

        verify(photoStorageService, times(1)).uploadPhotoshoot(mockMultipartFile);
        Order orderFromDb = ordersRepository.findFullOrderById(order.getId()).get();
        assertEquals(1, orderFromDb.getPhotoshoots().size());
        assertEquals(UPLOADED, orderFromDb.getState());
    }

    @Test
    public void registerApproval_whenOrderNotFound_throwsDomainNotFoundException() {
        assertThrows(DomainNotFoundException.class, () -> ordersService.registerApproval(10, true));
    }

    @Test
    public void registerApproval_whenApproved_marksPhotoshootApprovedAndMovesOrderToCompleted() {
        Photographer photographer = photographersRepository.save(createPhotographer());
        Order order = ordersRepository.save(createUploadedOrder(photographer));

        ordersService.registerApproval(order.getId(), true);

        Order orderFromDb = ordersRepository.findFullOrderById(order.getId()).get();
        assertTrue(orderFromDb.getPhotoshoots().get(0).getApproved());
        assertEquals(COMPLETED, orderFromDb.getState());
    }

    @Test
    public void registerApproval_whenRejected_marksPhotoshootRejectedAndMovesOrderToAssigned() {
        Photographer photographer = photographersRepository.save(createPhotographer());
        Order order = ordersRepository.save(createUploadedOrder(photographer));

        ordersService.registerApproval(order.getId(), false);

        Order orderFromDb = ordersRepository.findFullOrderById(order.getId()).get();
        assertFalse(orderFromDb.getPhotoshoots().get(0).getApproved());
        assertEquals(ASSIGNED, orderFromDb.getState());
    }
}
