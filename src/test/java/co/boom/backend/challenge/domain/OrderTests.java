package co.boom.backend.challenge.domain;

import co.boom.backend.challenge.common.exceptions.DomainException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static co.boom.backend.challenge.domain.OrderState.*;
import static co.boom.backend.challenge.utils.DomainFactory.*;
import static org.junit.jupiter.api.Assertions.*;

public class OrderTests {

    private Order order;

    @BeforeEach
    public void beforeEach() {
        order = createUnscheduledOrder();
    }

    @Test
    public void init_whenOrderHasId_throwsDomainException() {
        order.init();
        assertEquals(OrderState.UNSCHEDULED, order.getState());
    }

    @Test
    public void init_whenScheduleIsNull_setsStatusUnscheduled() {
        order.setId(null);
        order.init();
        assertEquals(OrderState.UNSCHEDULED, order.getState());
    }

    @Test
    public void init_whenScheduleIsNotNull_setsStatusPending() {
        order.setId(null);
        order.setSchedule(createSchedule());
        order.init();
        assertEquals(PENDING, order.getState());
    }

    @Test
    public void updateSchedule_whenWrongState_throwsDomainException() {
        order.setState(OrderState.UPLOADED);
        assertThrows(DomainException.class, () -> order.updateSchedule(createSchedule()));
    }

    @Test
    public void updateSchedule_whenUnscheduled_setsScheduleAndMovesOrderToPending() {
        order.setState(OrderState.UNSCHEDULED);
        Schedule schedule = createSchedule();

        order.updateSchedule(schedule);

        assertEquals(PENDING, order.getState());
        assertEquals(schedule, order.getSchedule());
    }

    @Test
    public void updateSchedule_whenAlreadyScheduled_allowsChangingSchedule() {
        order.setState(PENDING);
        order.setSchedule(createSchedule());
        Schedule newSchedule = createSchedule();

        order.updateSchedule(newSchedule);

        assertEquals(newSchedule, order.getSchedule());
    }

    @Test
    public void assignPhotographer_whenWrongState_throwsDomainException() {
        order.setState(UPLOADED);
        assertThrows(DomainException.class, () -> order.assignPhotographer(createPhotographer()));
    }

    @Test
    public void assignPhotographer_whenPending_setsPhotographerAndMovesOrderToAssigned() {
        order.setState(PENDING);
        Photographer photographer = createPhotographer();

        order.assignPhotographer(photographer);

        assertEquals(ASSIGNED, order.getState());
        assertEquals(photographer, order.getPhotographer());
    }

    @Test
    public void addPhotoshoot_whenWrongState_throwsDomainException() {
        order.setState(PENDING);
        assertThrows(DomainException.class, () -> order.addPhotoshoot(createPhotoshoot()));
    }

    @Test
    public void addPhotoshoot_whenAssigned_addsPhotoshootAndMovesOrderToUploaded() {
        order.setState(ASSIGNED);
        order.getPhotoshoots().add(createPhotoshoot());
        Photoshoot photoshoot = createPhotoshoot();

        order.addPhotoshoot(photoshoot);

        assertEquals(UPLOADED, order.getState());
        assertEquals(2, order.getPhotoshoots().size());
        assertEquals(photoshoot, order.getPhotoshoots().get(1));
    }

    @Test
    public void approve_whenWrongState_throwsDomainException() {
        order.setState(ASSIGNED);
        assertThrows(DomainException.class, () -> order.approve());
    }

    @Test
    public void approve_whenUploaded_marksApprovedLastUploadedPhotoshootAndCompletesOrder() {
        order.setState(UPLOADED);
        Photoshoot firstPhotoshoot = createPhotoshoot();
        firstPhotoshoot.setApproved(false);
        order.getPhotoshoots().add(firstPhotoshoot);
        order.getPhotoshoots().add(createPhotoshoot());

        order.approve();

        assertEquals(COMPLETED, order.getState());
        assertFalse(order.getPhotoshoots().get(0).getApproved());
        assertTrue(order.getPhotoshoots().get(1).getApproved());
    }

    @Test
    public void reject_whenWrongState_throwsDomainException() {
        order.setState(ASSIGNED);
        assertThrows(DomainException.class, () -> order.reject());
    }

    @Test
    public void approve_whenUploaded_marksRejectedLastUploadedPhotoshootAndMovesToAssigned() {
        order.setState(UPLOADED);
        Photoshoot firstPhotoshoot = createPhotoshoot();
        firstPhotoshoot.setApproved(false);
        order.getPhotoshoots().add(firstPhotoshoot);
        order.getPhotoshoots().add(createPhotoshoot());

        order.reject();

        assertEquals(ASSIGNED, order.getState());
        assertFalse(order.getPhotoshoots().get(0).getApproved());
        assertFalse(order.getPhotoshoots().get(1).getApproved());
    }

    @ParameterizedTest
    @EnumSource(OrderState.class)
    public void cancel_whateverTheOrderState_movesOrderToCanceled(OrderState state) {
        order.setState(state);
        order.cancel();
        assertEquals(CANCELED, order.getState());
    }

    @Test
    public void allOperations_whenOrderIsCanceled_throwDomainException() {
        order.setState(CANCELED);

        assertThrows(DomainException.class, () -> order.updateSchedule(createSchedule()));
        assertThrows(DomainException.class, () -> order.assignPhotographer(createPhotographer()));
        assertThrows(DomainException.class, () -> order.addPhotoshoot(createPhotoshoot()));
        assertThrows(DomainException.class, () -> order.reject());
        assertThrows(DomainException.class, () -> order.approve());
    }

}
