package co.boom.backend.challenge.controller;

import co.boom.backend.challenge.domain.Order;
import co.boom.backend.challenge.domain.Photographer;
import co.boom.backend.challenge.dto.CreateOrderDto;
import co.boom.backend.challenge.repository.ContactRepository;
import co.boom.backend.challenge.repository.OrdersRepository;
import co.boom.backend.challenge.repository.PhotographersRepository;
import co.boom.backend.challenge.utils.AssertionsUtils;
import co.boom.backend.challenge.utils.DomainFactory;
import co.boom.backend.challenge.utils.DtoFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static co.boom.backend.challenge.domain.OrderState.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class OrderControllerTests {

    public static final String BASE_RESOURCE_PATH = "/orders";
    public static final String ORDER_RESOURCE_PATH = "/orders/{id}";
    public static final String UPDATE_SCHEDULE_PATH = "/orders/{id}/schedule";
    public static final String ASSIGN_PHOTOGRAPHER_PATH = "/orders/{id}/photographer";
    public static final String UPLOAD_PATH = "/orders/{id}/photoshoots";
    public static final String APPROVAL_PATH = "/orders/{id}/approval";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper om;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private ContactRepository contactsRepository;

    @Autowired
    private PhotographersRepository photographersRepository;

    @AfterEach
    public void afterEach() {
        ordersRepository.deleteAll();
        contactsRepository.deleteAll();
        photographersRepository.deleteAll();
    }

    @Test
    public void createOrder_whenValidDto_createsOrderAndReturnsCreated() throws Exception {
        CreateOrderDto dto = DtoFactory.createPendingOrderDto();
        mockMvc.perform(post(BASE_RESOURCE_PATH).content(om.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        List<Order> orders = ordersRepository.findAll();
        assertEquals(1, orders.size());

        Order savedOrder = orders.get(0);
        AssertionsUtils.assertAreEqual(dto, savedOrder);
    }

    @Test
    public void createOrder_whenInvalidDto_returnsBadRequest() throws Exception {
        CreateOrderDto dto = DtoFactory.createUnscheduledOrderDto();
        dto.setPhotoType("wrongPhototype");
        mockMvc.perform(post(BASE_RESOURCE_PATH).content(om.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findOrderById_whenWrongId_returnsNotFound() throws Exception {
        mockMvc.perform(get(ORDER_RESOURCE_PATH, 99)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findOrderById_whenOrderExists_returnsFullOrder() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());
        Order order = ordersRepository.save(DomainFactory.createUploadedOrder(photographer));

        MvcResult mvcResult = mockMvc.perform(get(ORDER_RESOURCE_PATH, order.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String responseContent = mvcResult.getResponse().getContentAsString();
        Order actualOrder = om.readValue(responseContent, Order.class);
        assertNotNull(actualOrder.getSchedule());
        assertNotNull(actualOrder.getContact());
        assertFalse(actualOrder.getPhotoshoots().isEmpty());
    }

    @Test
    public void updateSchedule_whenWrongId_returnsNotFound() throws Exception {
        mockMvc.perform(put(UPDATE_SCHEDULE_PATH, 99)
                .content(om.writeValueAsString(DtoFactory.createScheduleDto()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateSchedule_whenWrongOrderState_returnsUnprocessableEntity() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());
        Order uploadedOrder = ordersRepository.save(DomainFactory.createUploadedOrder(photographer));

        mockMvc.perform(put(UPDATE_SCHEDULE_PATH, uploadedOrder.getId())
                .content(om.writeValueAsString(DtoFactory.createScheduleDto()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void updateSchedule_whenValidRequest_updatesOrderAndReturnsNoContent() throws Exception {
        Order unscheduledOrder = ordersRepository.save(DomainFactory.createUnscheduledOrder());

        mockMvc.perform(put(UPDATE_SCHEDULE_PATH, unscheduledOrder.getId())
                .content(om.writeValueAsString(DtoFactory.createScheduleDto()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        Order updatedOrder = ordersRepository.findById(unscheduledOrder.getId()).get();
        assertNotNull(updatedOrder.getSchedule());
        assertEquals(PENDING, updatedOrder.getState());
    }

    @Test
    public void assignPhotographer_whenWrongId_returnsNotFound() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());

        mockMvc.perform(put(ASSIGN_PHOTOGRAPHER_PATH, 99)
                .content(om.writeValueAsString(DtoFactory.createPhotographerAssignmentDto(photographer.getId())))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void assignPhotographer_whenWrongOrderState_returnsUnprocessableEntity() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());
        Order uploadedOrder = ordersRepository.save(DomainFactory.createUploadedOrder(photographer));

        mockMvc.perform(put(ASSIGN_PHOTOGRAPHER_PATH, uploadedOrder.getId())
                .content(om.writeValueAsString(DtoFactory.createPhotographerAssignmentDto(photographer.getId())))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void assignPhotographer_whenValidRequest_assignsPhotographerAndReturnsNoContent() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());
        Order pendingOrder = ordersRepository.save(DomainFactory.createPendingOrder());

        mockMvc.perform(put(ASSIGN_PHOTOGRAPHER_PATH, pendingOrder.getId())
                .content(om.writeValueAsString(DtoFactory.createPhotographerAssignmentDto(photographer.getId())))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        Order updatedOrder = ordersRepository.findById(pendingOrder.getId()).get();
        assertNotNull(updatedOrder.getPhotographer());
        assertEquals(photographer.getId(), updatedOrder.getPhotographer().getId());
        assertEquals(ASSIGNED, updatedOrder.getState());
    }

    @Test
    public void uploadPhotoshoot_whenWrongId_returnsNotFound() throws Exception {
        mockMvc.perform(multipart(UPLOAD_PATH, 99).file(mockMultipartFile()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void uploadPhotoshoot_whenWrongOrderState_returnsUnprocessableEntity() throws Exception {
        Order pendingOrder = ordersRepository.save(DomainFactory.createPendingOrder());

        mockMvc.perform(multipart(UPLOAD_PATH, pendingOrder.getId()).file(mockMultipartFile()))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void uploadPhotoshoot_whenValidRequest_savesPhotoshootAndReturnsCreated() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());
        Order assignedOrder = ordersRepository.save(DomainFactory.createAssignedOrder(photographer));

        mockMvc.perform(multipart(UPLOAD_PATH, assignedOrder.getId()).file(mockMultipartFile()))
                .andExpect(status().isCreated());

        Order updatedOrder = ordersRepository.findFullOrderById(assignedOrder.getId()).get();
        assertEquals(1, updatedOrder.getPhotoshoots().size());
        assertEquals(UPLOADED, updatedOrder.getState());
    }

    @Test
    public void registerApproval_whenWrongId_returnsNotFound() throws Exception {
        mockMvc.perform(post(APPROVAL_PATH, 99)
                .content(om.writeValueAsString(DtoFactory.createApprovalDto(true)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void registerApproval_whenApproved_movesOrderToCompletedAndReturnsNoContent() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());
        Order uploadedOrder = ordersRepository.save(DomainFactory.createUploadedOrder(photographer));

        mockMvc.perform(post(APPROVAL_PATH, uploadedOrder.getId())
                .content(om.writeValueAsString(DtoFactory.createApprovalDto(true)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        Order updatedOrder = ordersRepository.findFullOrderById(uploadedOrder.getId()).get();
        assertEquals(COMPLETED, updatedOrder.getState());
    }

    @Test
    public void registerApproval_whenRejected_movesOrderToAssignedAndReturnsNoContent() throws Exception {
        Photographer photographer = photographersRepository.save(DomainFactory.createPhotographer());
        Order uploadedOrder = ordersRepository.save(DomainFactory.createUploadedOrder(photographer));

        mockMvc.perform(post(APPROVAL_PATH, uploadedOrder.getId())
                .content(om.writeValueAsString(DtoFactory.createApprovalDto(false)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        Order updatedOrder = ordersRepository.findFullOrderById(uploadedOrder.getId()).get();
        assertEquals(ASSIGNED, updatedOrder.getState());
    }

    @Test
    public void cancelOrder_whenWrongId_returnsNotFound() throws Exception {
        mockMvc.perform(delete(ORDER_RESOURCE_PATH, 99)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void cancelOrder_whenOrderExists_movesOrderToCanceledAndReturnsNoContent() throws Exception {
        Order order = ordersRepository.save(DomainFactory.createPendingOrder());

        mockMvc.perform(delete(ORDER_RESOURCE_PATH, order.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        Order updatedOrder = ordersRepository.findFullOrderById(order.getId()).get();
        assertEquals(CANCELED, updatedOrder.getState());
    }

    private MockMultipartFile mockMultipartFile() {
        return new MockMultipartFile("file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

    }
}
