package co.boom.backend.challenge.common.validation;

import co.boom.backend.challenge.domain.Schedule;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ScheduleValidatorTests {

    private Validator validator;

    @BeforeEach
    public void before() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @ParameterizedTest
    @MethodSource("validTimes")
    public void validation_whenValid_returnsTrue(int hour, int minute) {
        Schedule schedule = new Schedule();
        schedule.setDateTime(LocalDateTime.of(2021, 2, 27, hour, minute));
        schedule.setTimezone("Europe/Rome");

        Set<ConstraintViolation<Schedule>> constraintViolations = validator.validate(schedule);
        assertEquals(0, constraintViolations.size());
    }

    private static Stream<Arguments> validTimes() {
        return Stream.of(
                arguments(19, 0),
                arguments(7, 0),
                arguments(12, 0)
        );
    }

    @Test
    public void validation_whenWrongTimezone_returnsFalse() {
        Schedule schedule = new Schedule();
        schedule.setDateTime(LocalDateTime.of(2021, 2, 27, 15, 0));
        schedule.setTimezone("wrong");

        Set<ConstraintViolation<Schedule>> constraintViolations = validator.validate(schedule);
        assertNotEquals(0, constraintViolations.size());
    }

    @ParameterizedTest
    @MethodSource("invalidTimes")
    public void validation_whenDateTimeOutsideBusinessHours_returnsFalse(int hour, int minute) {
        Schedule schedule = new Schedule();
        schedule.setDateTime(LocalDateTime.of(2021, 2, 27, hour, minute));
        schedule.setTimezone("Europe/Rome");

        Set<ConstraintViolation<Schedule>> constraintViolations = validator.validate(schedule);
        assertNotEquals(0, constraintViolations.size());
    }

    private static Stream<Arguments> invalidTimes() {
        return Stream.of(
                arguments(19, 1),
                arguments(6, 59),
                arguments(3, 0)
        );
    }
}
